# Networked warehouse-c

A system to allow networking of warehouses with transferral of goods

Run Makefile on local machine to create 2310depot
    type "make" in the directory of files

Start an instance of 2310depot with name and resource amount pair 
    (repeated for each resource)

./2310depot {name} {resource} {amount} {resource} {amount} ...

When a depot recieves a SIGHUP message it will print out a list of goods it
holds and it's neighbours (neighbours are the connected depots)

Once a depot is running it can be sent messages:

| Message | Parameters | Action |
| ------ | ------ | ------ |
| Connect:p | p : port to connect to | Connect to another depot |
| IM:p:name | p : the command port of the depot sending the message name : name of the other depot | Another depot has connected to you and is introducing itself. Both parties should send one of these immediately after a new connection |
| Deliver:q:t | q : quantity of good t : type of good | Add the quantity of the good to your stocks. |
| Withdraw:q:t | q : quantity of good t : type of good | Remove the quantity of the good from your stocks. |
| Transfer:q:t:dest | dest : name of depot to send to | As withdraw but send a delivery message to the named depot for that quantity and type of goods. |
| Defer:k:Deliver:q:t | k : key to refer to later | As with Delivery but deferred until later |
| Defer:k:Withdraw:q:t | k : key to refer to later | As with Withdraw but deferred until later |
| Defer:k:Transfer:q:t:dest | k : key to refer to later | As with Transfer but deferred until later |
| Execute:k | k : key for deferred tasks to be executed | Carry out all deferred orders with key k |

