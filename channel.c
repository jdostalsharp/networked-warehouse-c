#include "channel.h"
#include "queue.h"

/* Creates a channel that has a queue and a mutex lock and semaphore
 * returns a struct
 */
struct Channel new_channel(void) {
    struct Channel output;
    pthread_mutex_init(&output.lock, NULL);
    output.inner = new_queue();
    sem_init(&output.signal, 0, 0);
    return output;
}

/* Destroys a channel
 */
void destroy_channel(struct Channel* channel, void (*clean)(void*)) {
    destroy_queue(&channel->inner, clean);
    pthread_mutex_destroy(&channel->lock);
    sem_destroy(&channel->signal);
}

/* creates a write "end" of a channel
 * returns true if succesful
 */
bool write_channel(struct Channel* channel, void* data) {
    pthread_mutex_lock(&channel->lock);
    bool output = write_queue(&channel->inner, data);
    pthread_mutex_unlock(&channel->lock);
    sem_post(&channel->signal);
    return output;
}

/* Creates a read "end" of a channel
 * returns true if succesful
 */
bool read_channel(struct Channel* channel, void** out) {
    sem_wait(&channel->signal);
    pthread_mutex_lock(&channel->lock);
    bool output = read_queue(&channel->inner, out);
    pthread_mutex_unlock(&channel->lock);
    return output;
}
