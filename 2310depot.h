#ifndef DEPOT_H
#define DEPOT_H

#define MAXNAME 100
#define MAX_INPUT 80
#define MAX_R_NAME 80
#define NEIGH_STORAGE 100
#define DEPOT_START_STORAGE 10
#define MESSAGE_COMMAND 10
#define KEY_SIZE 10
#include <stdbool.h>
#include <stdio.h>
#include "channel.h"

typedef struct {
    FILE* to;
    FILE* from;
    char* name;
    int port;
    pthread_t tid;
    int threadFD;
    struct Channel* channel;
} Neighbour;

typedef struct {
    char* key;
    char** messages;
    int messageCount;
    int messageSpace;
} Defer;

typedef struct {
    char* name;
    int qty;
} Resource;

typedef struct {
    int port;
    char* name;
    volatile int stockpileHolding;
    int connections;
    volatile int neighbourCount;
    int stockpileSize;
    Neighbour* neighbours;
    pthread_t networkid;
    Resource** stockpile;
    struct Channel channel;
    Defer** deferred;
    int deferredCount;
    int deferredTotal;
    int connectBuffer;
} Depot;

typedef struct {
    char* port;
    Depot* depotCurrent;
} Client;

typedef enum {
    OK = 0,
    INCORRECTARGS = 1,
    INVALIDNAME = 2,
    INVALIDQTY = 3
} DepotStatus;

typedef enum {
    CONNECT = 10,
    IM = 11,
    DELIVER = 12,
    WITHDRAW = 13,
    TRANSFER = 14,
    DEFER = 15,
    EXECUTE = 16,
    ERROR = 17,
    NOERROR = 18
} MessageStatus;

void print_keys(Depot* depot);

void set_up_sigaction(void);

void print_sighup(int s);

void print_nonzero_stock(Depot* depot);

void print_neighbours(Depot* depot);

DepotStatus show_message(DepotStatus s);

DepotStatus check_name(char* name);

DepotStatus check_args(int argCount, char** arguments);

void set_depot_name(Depot* depot, char* name);

void initialise_depot(Depot* depot);

void check_deferred_count(Depot* depot);

void check_neighbour_storage(Depot* depot);

void check_stockpile_storage(Depot* depot);

MessageStatus process_im(char* message, Neighbour* neighbour);

MessageStatus process_message(char* message, Depot* depot);

MessageStatus execute_messages(char* key, Depot* depot);

MessageStatus defer_message_key(char* message, Depot* depot);

int defer_message(char* key, char* message, Depot* depot);

Defer* create_deferred_list();

void check_deferred_space(Defer* deferred);

void destroy_deferred(Defer* deferred);

MessageStatus transfer_request(char* message, Depot* depot);

MessageStatus transfer(char* dest, char* resource, int qty, Depot* depot);

MessageStatus change_stock(char* message, Depot* depot);

MessageStatus change_stock_withdraw(char* message, Depot* depot);

bool check_deliver(char* msg);

bool check_defer(char* msg);

bool check_withdraw(char* msg);

bool check_transfer(char* msg);

bool check_execute(char* msg);

bool check_connect(char* temp);

MessageStatus create_connection(char* port, Depot* depot);

void* create_neighbour(void* depotDetails);

void* create_server(void* depotInfo);

void* create_client_connection(void* clientInfo);

void neighbour_thread(Depot* depot, FILE* to, FILE* from);

Neighbour* allocate_neighbour();

bool get_neighbour_port(Neighbour* neighbour, char* port);

bool get_neighbour_name(Neighbour* neighbour, char* name);

Resource* create_resource();

int add_resource(char* name, int qty, Depot* depot);

DepotStatus add_start_to_stockpile(int argTotal, char** args, Depot* depot);

void print_resources(Depot* depot);

void print_neighbour_names(Depot* depot);

int comp_resource_name(const void* v1, const void* v2);

int comp_neighbour_name(const void* v1, const void* v2);

Client* create_client_info(Depot* depot, char* port);

#endif

