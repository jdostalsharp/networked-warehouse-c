#include "queue.h"
#include <stdlib.h>

// Starting length of the queue.
const int queueCapacity = 20;

/* Creates a gueue struct setting the memory needed
 * returns Queue
 */
struct Queue new_queue(void) {
    struct Queue output;

    output.data = malloc(sizeof(void*) * queueCapacity);
    output.readEnd = -1; // queue is empty
    output.writeEnd = 0; // put first piece of data at the start of the queue

    return output;
}

/* Destroys a Queue
 */
void destroy_queue(struct Queue* queue, void (*clean)(void*)) {
    void* data;
    while (read_queue(queue, &data)) {
        clean(data);
    }

    free(queue->data);
}

/* Writes something into the "end of the queue
 * returns true if successful
 * false if queue is full
 */
bool write_queue(struct Queue* queue, void* data) {
    if (queue->writeEnd == queue->readEnd) {
        // queue is full
        //expand_queue(queue);
        return false;
    }

    queue->data[queue->writeEnd] = data;

    // if queue was empty, signal that the queue is no longer empty
    if (queue->readEnd == -1) {
        queue->readEnd = queue->writeEnd;
    }

    queue->writeEnd = (queue->writeEnd + 1) % queueCapacity;

    return true;
}

/* Reads something from the beginning of the queue
 * Returns true if succesful
 * false if queue is empty
 */
bool read_queue(struct Queue* queue, void** output) {
    if (queue->readEnd == -1) {
        // queue is empty
        return false;
    }

    *output = queue->data[queue->readEnd];

    queue->readEnd = (queue->readEnd + 1) % queueCapacity;

    if (queue->readEnd == queue->writeEnd) {
        queue->readEnd = -1;
    }

    return true;
}

