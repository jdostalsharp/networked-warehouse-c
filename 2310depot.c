#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <pthread.h>

#include "2310depot.h"

//Pointer to the depot struct that can be used for a SIGHUP
Depot* globalDepot;


//Set up sigaction
void set_up_sigaction(void) {
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = print_sighup;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGHUP, &sa, 0);
    sa.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &sa, 0);
}

//Prints the stock then neighbours in lexagraphic order
void print_sighup(int s) {
    print_nonzero_stock(globalDepot);
    print_neighbours(globalDepot);
}

// Prints nonzero stock in lexagraphic order
void print_nonzero_stock(Depot* depot) {
    fprintf(stdout, "Goods:\n");
    fflush(stdout);
    print_resources(depot);
}

//Prints neighbours in lexagraphic order
void print_neighbours(Depot* depot) {
    fprintf(stdout, "Neighbours:\n");
    fflush(stdout);
    print_neighbour_names(depot);
}

//Outputs error message for status and returns DepotStatus
DepotStatus show_message(DepotStatus s) {
    const char* message[] = {"",
            "Usage: 2310depot name {goods qty}\n",
            "Invalid name(s)\n",
            "Invalid quantity\n"};
    fputs(message[s], stderr);
    return s;
}

/* Checks the name argument to see if it contains illegal characters
 * Return OK if it doesn't or INVALIDNAME if it does.
 */
DepotStatus check_name(char* name) {
    int i = 0;
    if (name == NULL) {
        return INVALIDNAME;
    }
    while ((i < MAXNAME) && name[i] != '\0') {
        if ((name[i] == ' ') || (name[i] == '\n') || (name[i] == '\r')
                || (name[i] == ':')) {
            return INVALIDNAME;
        }
        i++;
    }
    if (i == 0) {
        return INVALIDNAME;
    }
    return OK;
}

/* Check Depot arguments
 * Returns DepotStatus OK if everything is good.
 * Otherwise return DepotStatus error
 */
DepotStatus check_args(int argCount, char** arguments) {
    if ((argCount < 2) || (argCount % 2 != 0)) {
        return INCORRECTARGS;
    }
    
    DepotStatus result = check_name(arguments[1]);
    
    if (result != OK) { 
        return result;
    }

    char* err;
    for (int i = 2; i < argCount; i++) {
        if (i % 2 != 0) {
            if ((arguments[i] == '\0') || (arguments[i] == NULL) 
                    || (!(strcmp(arguments[i], "")))) {
                return INVALIDQTY;
            }
            int argToCheck = strtol(arguments[i], &err, 10);
            if (*err != '\0') {
                return INVALIDQTY;
            }
            if (argToCheck < 0) {
                return INVALIDQTY;
            }
        }
    }
    return OK;
}

/* Check and process the IM message and set neighbour parameters
 * Caller is responsible for freeing memory
 * Returns MessageStatus IM if it was correct ERROR if something went wrong
 */
MessageStatus process_im(char* message, Neighbour* neighbour) {
    char* temp, *save;
    char* iMessage;
    //save = malloc(sizeof(char*));
    temp = strtok_r(message, ":", &save);

    if (temp == NULL) {
        return ERROR;
    }

    iMessage = malloc(sizeof(char) * MESSAGE_COMMAND);
    strcpy(iMessage, temp);

    if ((iMessage[0] == 'I') && (iMessage[1] == 'M') 
            && (iMessage[2] == '\0')) {
        // Get the port number from the IM message or return ERROR
        temp = strtok_r(save, ":", &save);
        if (temp == NULL) {
            return ERROR;
        }
        //neighbour->port = malloc(sizeof(char) * 10);
        if (!(get_neighbour_port(neighbour, temp))) {
            return ERROR;
        }
        // Get the name from the IM message or return ERROR
        temp = strtok_r(save, "\n", &save);
        if (temp == NULL) {
            return ERROR;
        }
        //neighbour->name = malloc(sizeof(char) * MAXNAME);
        if (!(get_neighbour_name(neighbour, temp))) {
            return ERROR;
        }
        
        // Everything worked
        return IM;
    }
    
    return ERROR;
}

/*Process a message and call message function to deal with each type
 * of message.
 * return ERROR if there is an issue, NOERROR if everything is fine
 */
MessageStatus process_message(char* message, Depot* depot) {
    char* temp, *save;
    char* msg;
    MessageStatus result = ERROR;

    temp = strtok_r(message, ":", &save);

    if (temp == NULL) {
        return result;
    }

    msg = malloc(sizeof(char) * MESSAGE_COMMAND);
    strcpy(msg, temp);

    if (check_deliver(msg)) {
        result = change_stock(save, depot);
    } else if (check_defer(msg)) {
        result = defer_message_key(save, depot);
    } else if (check_withdraw(msg)) {
        result = change_stock_withdraw(save, depot);
    } else if (check_transfer(msg)) {
        result = transfer_request(save, depot);
    } else if (check_execute(msg)) {
        result = execute_messages(save, depot);
    } else if (check_connect(msg)) {
        result = create_connection(save, depot);
    }
    return result;
}

/* Process an execut message, if key corresponds to a list of deferred
 * messages. Execute all messages in the list.
 * Return NOERROR on success
 * ERROR on fail
 */
MessageStatus execute_messages(char* key, Depot* depot) {
    for (int i = 0; i < depot->deferredCount; i++) {
        if (!(strcmp(depot->deferred[i]->key, key))) {
            for (int j = 0; j < depot->deferred[i]->messageCount; j++) {
                process_message(depot->deferred[i]->messages[j], depot);
            }
            destroy_deferred(depot->deferred[i]);
            return NOERROR;
        }
    }
    return ERROR;
}

/*Gets a key from a defer message and calls defer_message
 * returns NOERROR if everything went ok,
 * ERROR if something bad happened...
 */
MessageStatus defer_message_key(char* message, Depot* depot) {
    char* key, *save;
    key = malloc(sizeof(char) * KEY_SIZE);
    key = strtok_r(message, ":", &save);
    if (key == NULL) {
        return ERROR;
    }
    char* err;
    strtoul(key, &err, 10);
    if (*err != '\0') {
        return ERROR;
    }
    defer_message(key, save, depot);
    return NOERROR;
}

/* Process a defer message, save the message to a list with an ID
 * returns 0 on completion
 */
int defer_message(char* key, char* message, Depot* depot) {
    int i = 0;
    for (; i < depot->deferredCount; i++) {
        if (!(strcmp(depot->deferred[i]->key, key))) {
            strcpy(depot->deferred[i]->messages[depot->
                    deferred[i]->messageCount], message);
            check_deferred_space(depot->deferred[i]);
            depot->deferred[i]->messageCount++;
            return 0;
        }
    }
    depot->deferred[i] = create_deferred_list();
    strcpy(depot->deferred[i]->key, key);
    strcpy(depot->deferred[i]->messages[0], message);
    depot->deferred[i]->messageCount++;
    depot->deferredCount++;
    check_deferred_count(depot);
    return 0;
}

/* Sets aside memory for a deferred struct
 */
Defer* create_deferred_list() {
    Defer* deferred = malloc(sizeof(Defer));
    deferred->messageCount = 0;
    deferred->messageSpace = KEY_SIZE;
    deferred->key = malloc(sizeof(char) * KEY_SIZE);
    deferred->messages = (char**) malloc(sizeof(char*) * 
            deferred->messageSpace);
    for (int i = 0; i < deferred->messageSpace; i++) {
        deferred->messages[i] = malloc(sizeof(char) * 
                (MAX_INPUT + MAX_R_NAME));
    }
    return deferred;
}

/* Check to see if there is enough space to put a deferred message
 * if there is not reallocate more space. This will create more space if
 * the total messages stored is one less than the max
 */
void check_deferred_space(Defer* deferred) {
    if (deferred->messageCount == deferred->messageSpace - 1) {
        deferred->messageSpace *= 2;
        deferred->messages = realloc(deferred->messages, 
                deferred->messageSpace);
        for (int i = deferred->messageCount; i < deferred->messageSpace;
                i++) {
            deferred->messages[i] = malloc(sizeof(char) * 
                    MAX_INPUT + MAX_R_NAME);
        }
    }
}

/* Destroy a Defer struct
 */
void destroy_deferred(Defer* deferred) {
    for (int i = 0; i < deferred->messageCount; i++) {
        free(deferred->messages[i]);
    }
    deferred->messageCount = 0;
}

/* Process a transfer message and call transfer() on neighbour
 * returns NOERROR if successful, ERROR if failed
 */
MessageStatus transfer_request(char* message, Depot* depot) {
    char* temp, *save;
    char* err;

    temp = strtok_r(message, ":", &save);
    if (temp == NULL) {
        return ERROR;
    }

    int qty = strtol(temp, &err, 10);
    if (*err != '\0') {
        return ERROR;
    }

    temp = strtok_r(save, ":", &save);
    if (temp == NULL) {
        return ERROR;
    }

    MessageStatus result = transfer(save, temp, qty, depot);
    return result;
}

/* Send a Deliver message to the dest, with q = qty & t = resource
 * returns NOERROR on success, ERROR if there is an issue.
 */
MessageStatus transfer(char* dest, char* resource, int qty, Depot* depot) {
    for (int i = 0; i < depot->neighbourCount; i++) {
        if ((strcmp(dest, depot->neighbours[i].name)) == 0) {
            fprintf(depot->neighbours[i].to, "Deliver:%d:%s\n", 
                    qty, resource);
            fflush(depot->neighbours[i].to);
            add_resource(resource, -qty, depot);
            return NOERROR;
        }
    }
    return ERROR;
}

/* Process a delivery message
 * returns NOERROR if successful
 * ERROR if delivery details were incorrect
 */
MessageStatus change_stock(char* message, Depot* depot) {
    char* temp, *save;
    char* err;

    temp = strtok_r(message, ":", &save);

    if (temp == NULL) {
        return ERROR;
    }

    int qty = strtol(temp, &err, 10);
    if (*err != '\0') {
        return ERROR;
    }

    if (save == NULL) {
        return ERROR;
    }
    
    char* name = malloc(sizeof(char) * MAX_R_NAME);
    name = strtok_r(save, "\n", &save);

    if (name == NULL) {
        return ERROR;
    }
    
    add_resource(name, qty, depot);
    
    return NOERROR;
}

/* Process a Withdraw message
 * returns NOERROR if successful
 * ERROR if delivery details were incorrect
 */
MessageStatus change_stock_withdraw(char* message, Depot* depot) {
    char* temp, *save;
    char* err;

    temp = strtok_r(message, ":", &save);

    if (temp == NULL) {
        return ERROR;
    }

    int qty = strtol(temp, &err, 10);
    if (*err != '\0') {
        return ERROR;
    }

    if (save == NULL) {
        return ERROR;
    }

    char* name = malloc(sizeof(char) * MAX_R_NAME);
    name = strtok_r(save, "\n", &save);

    if (name == NULL) {
        return ERROR;
    }
    add_resource(name, -qty, depot);

    return NOERROR;
}

/* Checks a char* message
 * Returns true if the char* is Deliver
 * false if anything else
 */
bool check_deliver(char* msg) {
    if ((msg[0] == 'D') && (msg[1] == 'e') && (msg[2] == 'l') && 
            (msg[3] == 'i') && (msg[4] == 'v') && (msg[5] == 'e') 
            && (msg[6] == 'r') && (msg[7] == '\0')) {
        return true;
    }
    return false;
}

/* Checks a char* message
 * Returns true if the char* is Defer
 * false if anything else
 */
bool check_defer(char* msg) {
    if ((msg[0] == 'D') && (msg[1] == 'e') && (msg[2] == 'f') 
            && (msg[3] == 'e') && (msg[4] == 'r') && (msg[5] == '\0')) {
        return true;
    }
    return false;
}

/* Checks a char* message
 * Returns true if the char* is Withdraw
 * false if anything else
 */
bool check_withdraw(char* msg) {
    if ((msg[0] == 'W') && (msg[1] == 'i') && (msg[2] == 't') 
            && (msg[3] == 'h') && (msg[4] == 'd') && (msg[5] == 'r') 
            && (msg[6] == 'a') && (msg[7] == 'w') && (msg[8] == '\0')) {
        return true;
    }
    return false;
}
 
/* Checks a char* message
 * Returns true if the char* is Transfer
 * false if anything else
 */
bool check_transfer(char* msg) {
    if ((msg[0] == 'T') && (msg[1] == 'r') && (msg[2] == 'a') 
            && (msg[3] == 'n') && (msg[4] == 's') && (msg[5] == 'f') 
            && (msg[6] == 'e') && (msg[7] == 'r') && (msg[8] == '\0')) {
        return true;
    }
    return false;
}

/* Checks a char* message
 * Returns true if the char* is Execute
 * false if anything else
 */
bool check_execute(char* msg) {
    if ((msg[0] == 'E') && (msg[1] == 'x') && (msg[2] == 'e') && 
            (msg[3] == 'c') && (msg[4] == 'u') && (msg[5] == 't') 
            && (msg[6] == 'e') && (msg[7] == '\0')) {
        return true;
    }
    return false;
}

/* Check to see if a message is connect
 * returns true if so
 * false otherwise
 */
bool check_connect(char* temp) {
    if ((temp[0] == 'C') && (temp[1] == 'o') && (temp[2] == 'n') &&
            (temp[3] == 'n') && (temp[4] == 'e') && (temp[5] == 'c')
            && (temp[6] == 't') && (temp[8] == '\0')) {
        return true;
    }
    return false;
}

/* Passes the a client struct to the create_client_connection
 * with the info needed.
 */
MessageStatus create_connection(char* port, Depot* depot) {
    bool neighbourExists = false;
    char* err;
    int portAsNum = strtol(port, &err, 10);
    if (*err != '\0') {
        return ERROR;
    }
    if (portAsNum == depot->port) {
        return ERROR;
    }
    if ((port != NULL) && (strcmp(port, "")) && (port != '\0')) {
        for (int i = 0; i < depot->neighbourCount; i++) {
            if (depot->neighbours[i].port == portAsNum) {
                neighbourExists = true;
            }
        }
        if (portAsNum == depot->connectBuffer) {
            neighbourExists = true;
        }
        if (!(neighbourExists)) {
            Client* client = create_client_info(depot, port);
            create_client_connection(client);
            depot->connectBuffer = portAsNum;
            return NOERROR;
        }
    }
    return ERROR;
}

// Sets this depot name
void set_depot_name(Depot* depot, char* name) {
    depot->name = name;
}

//Initialise a Depot struct
void initialise_depot(Depot* depot) {
    depot->port = 0;
    depot->neighbourCount = 0;
    depot->connectBuffer = 0;
    depot->connections = NEIGH_STORAGE;
    depot->stockpileSize = DEPOT_START_STORAGE;
    depot->stockpileHolding = 0;
    depot->name = malloc(sizeof(char) * MAXNAME);
    depot->neighbours = (Neighbour*)
            malloc(sizeof(Neighbour*) * depot->connections);
    depot->stockpile = (Resource**) malloc(sizeof(Resource*) * 
            depot->stockpileSize);
    depot->channel = new_channel();
    depot->deferredCount = 0;
    depot->deferredTotal = KEY_SIZE;
    depot->deferred = malloc(sizeof(Defer*) * depot->deferredTotal);
}

/* Allocate more space for deferred key structs if needed.
 */
void check_deferred_count(Depot* depot) {
    if (depot->deferredCount >= depot->deferredTotal - 1) {
        depot->deferredTotal *= 2;
        depot->deferred = realloc(depot->deferred, depot->deferredTotal);
    }
}

/* Check to see if we have enough storage space for a new neighbour
 * Reallocate more space for neighbours if it is needed
 */
void check_neighbour_storage(Depot* depot) {
    if (!(depot->neighbourCount < depot->connections)) {
        depot->neighbours = realloc(depot->neighbours, sizeof(Neighbour*) 
                * (2 * depot->connections));
        depot->connections *= 2;
    }
}

/* Check to see if we need to expand the stockpile to hold more resource
 * types.
 * Reallocates more space if needed.
 */
void check_stockpile_storage(Depot* depot) {
    if (!(depot->stockpileHolding < depot->stockpileSize)) {
        depot->stockpile = realloc(depot->stockpile, sizeof(Resource*) *
                (2 * depot->stockpileSize));
        depot->stockpileSize *= 2;
    }
}

/* When a connection is achieved this will set
 * neighbour details process the neighbours IM message
 * Then conitnue to wait for new messages and send them channel
 * thread ends on return
 */
void* create_neighbour(void* neighbourDetails) {
    Neighbour* neighbour = (Neighbour*) neighbourDetails;
    char buffer[MAX_INPUT];
    char buffer2[MAX_INPUT];
    char* string;

    fgets(buffer, MAX_INPUT - 1, neighbour->from);
    MessageStatus result = process_im(buffer, neighbour);
    if (result == ERROR) {
        return NULL;
    }
    
    while ((string = fgets(buffer2, MAX_INPUT - 1, neighbour->from))) {
     
        if (string[strlen(string) - 1] == '\n') {
            string[strlen(string) - 1] = '\0';
        }
        write_channel(neighbour->channel, (void*) strdup(string));
        //Sanitize buffer2
        for (int i = 0; i < MAX_INPUT; i++) {
            buffer2[i] = '\0';
        }
    }

    return NULL;
}

/* Creates a server which can take connections from a neighbour and sets 
 * the FILE* of that neighbour to the read and write of that port
 */
void* create_server(void* depotInfo) {
    Depot* depot = (Depot*) depotInfo;
    struct addrinfo* ai = 0;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    int err = getaddrinfo("127.0.0.1", 0, &hints, &ai);
    if (err) {
        freeaddrinfo(ai);
        //fprintf(stderr, "%s\n", gai_strerror(err));
        return NULL;
    }
    
    //Create Socket and bind to port
    int serv = socket(AF_INET, SOCK_STREAM, 0);
    if (bind(serv, (struct sockaddr*)ai->ai_addr, sizeof(struct sockaddr))) {
        //perror("Binding");
        return NULL;
    }

    //Which port did we get
    struct sockaddr_in ad;
    memset(&ad, 0, sizeof(struct sockaddr_in));
    socklen_t len = sizeof(struct sockaddr_in);
    if (getsockname(serv, (struct sockaddr*)&ad, &len)) {
        //perror("sockname");
        return NULL;
    }
    fprintf(stdout, "%u\n", ntohs(ad.sin_port));
    depot->port = ntohs(ad.sin_port);
    fflush(stdout);

    if (listen(serv, 10)) {
        //perror("Listen");
        return NULL;
    }

    int connFd;
    while (connFd = accept(serv, 0, 0), connFd >= 0) {
        int connFd2 = dup(connFd);
        FILE* to = fdopen(connFd, "w");
        FILE* from = fdopen(connFd2, "r");
        fprintf(to, "IM:%u:%s\n", ntohs(ad.sin_port), depot->name);
        fflush(to);
        neighbour_thread(depot, to, from);
    }

    return NULL;
}

/* Connects to port as a client then creates a neighbour for the server
 * connected to
 */
void* create_client_connection(void* clientInfo) {
    Client* client = (Client*) clientInfo;
    
    const char* port = client->port; 
    struct addrinfo* ai = 0;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    int err;
    if ((err = getaddrinfo("127.0.0.1", port, &hints, &ai))) {
        freeaddrinfo(ai);
        //fprintf(stderr, "%s\n", gai_strerror(err));
        return NULL;
    }

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (connect(fd, (struct sockaddr*)ai->ai_addr, 
            sizeof(struct sockaddr))) {
        //perror("Connecting");
        return NULL;
    }

    int fd2 = dup(fd);
    FILE* to = fdopen(fd, "w");
    FILE* from = fdopen(fd2, "r");
    fprintf(to, "IM:%u:%s\n", client->depotCurrent->port, 
            client->depotCurrent->name);
    fflush(to);
    neighbour_thread(client->depotCurrent, to, from);

    return NULL;
}

/* Checks neighbour storage adds the new neighbour connection to the list 
 * of neighbours sets FILE* for the neighbour creates a new thread for this
 * neighbour
 */
void neighbour_thread(Depot* depot, FILE* to, FILE* from) {
    Neighbour* neighbour = allocate_neighbour();

    neighbour->to = to;
    neighbour->from = from;
    neighbour->channel = &depot->channel;
    depot->neighbours[depot->neighbourCount] = *neighbour;
        
    pthread_create(&neighbour->tid, 0, create_neighbour, 
            &depot->neighbours[depot->neighbourCount]);
    depot->neighbourCount++; 
    check_neighbour_storage(depot);
}

/* Create memory for a neighbour struct
 * returns a pointer to the struct
 */
Neighbour* allocate_neighbour() {
    Neighbour* neighbour = malloc(sizeof(Neighbour));
    neighbour->name = malloc(sizeof(char) * MAXNAME);
    for (int i = 0; i < MAXNAME; i++) {
        neighbour->name[i] = '\0';
    }
    return neighbour;
}

/* Gets the port of the new connection from the initial IM message and 
 * sets neigbour->name port to it.
 * Return 0 on success, 1 on fail.
 */
bool get_neighbour_port(Neighbour* neighbour, char* port) {
    char* err;
    // Check to see if it is a valid number
    int portNum = strtoul(port, &err, 10);
     
    if (*err != '\0') {
        return false;
    }
  
    neighbour->port = portNum;
    return true;
}

/* Gets the name of the neighbour checks to see if it is valid then stores
 * it in neigbours name field. 
 * returns 0 on success or 1 if failed.
 */
bool get_neighbour_name(Neighbour* neighbour, char* name) {
    DepotStatus result = check_name(name);
    if (result != OK) {
        return false;
    }

    strcpy(neighbour->name, name);
    return true;
}

/* Allocates memory for a resource
 * returns a Resource*
 */
Resource* create_resource() {
    Resource* resource = malloc(sizeof(Resource));
    resource->name = malloc(sizeof(char) * MAX_R_NAME);
    return resource;
}

/* Checks to see if a resource already exists if so it adds the qty to it
 * otherwise it creates a new resource and sets it's qty
 * returns 0
 */
int add_resource(char* name, int qty, Depot* depot) {
    int i = 0;
    DepotStatus result = check_name(name);
    if (result != OK) {
        return 0;
    }
    for (; i < depot->stockpileHolding; i++) {
        if (!(strcmp(depot->stockpile[i]->name, name))) {
            depot->stockpile[i]->qty += qty;
            return 0;
        }
    }
    depot->stockpile[i] = create_resource();
    strcpy(depot->stockpile[i]->name, name);
    depot->stockpile[i]->qty = qty;
    depot->stockpileHolding++;
    check_stockpile_storage(depot);
    return 0;
}

/* Checks each resource argument name and quantity pair and stores it
 * in the stockpile.
 * returns DepotStatus OK if good or INVALIDNAME, INVALIDQTY
 */
DepotStatus add_start_to_stockpile(int argTotal, char** args, 
        Depot* depot) {
    int counter = 2;
    while (counter < argTotal) {
        DepotStatus result = check_name(args[counter]);
        if (result != OK) {
            return result;
        }
        char* err;
        unsigned int quantity = strtoul(args[(counter + 1)], &err, 10);
        if (*err != '\0') {
            return INVALIDQTY;
        }
        add_resource(args[counter], quantity, depot);
        counter += 2;
    }
    return OK;
}

/* Iterate through the list of resources and print out their name:quantity
 * pair
 */
void print_resources(Depot* depot) {
    int i = 0;
    Resource* resources[depot->stockpileHolding];
    //Add all the resources in the depot to an array to be passed to qsort
    while (i < depot->stockpileHolding) {
        resources[i] = depot->stockpile[i];
        i++;
    }
    
    qsort(resources, depot->stockpileHolding, 
            sizeof(Resource*), comp_resource_name);

    for (int j = 0; j < depot->stockpileHolding; j++) {
        if (resources[j]->qty != 0) {
            fprintf(stdout, "%s %d\n", resources[j]->name,
                    resources[j]->qty);
            fflush(stdout);
        }
    }
}

/* Iterate through the list of neighbours and print out their name
 */
void print_neighbour_names(Depot* depot) {
    int i = 0;
    Neighbour* neighbours[depot->neighbourCount];
    //Add all the neighbours in the depot to an array to be passed to qsort
    while (i < depot->neighbourCount) {
        neighbours[i] = &depot->neighbours[i];
        i++;
    }
    
    qsort(neighbours, depot->neighbourCount, 
            sizeof(Neighbour*), comp_neighbour_name);

    for (int j = 0; j < depot->neighbourCount; j++) {
        if (neighbours[j]->name != '\0') {
            fprintf(stdout, "%s\n", neighbours[j]->name);
            fflush(stdout);
        }
    }
}

/* Sort the resources by name
 * Return > 0 if resource1 name < resource2 name
 * < 0 if resource1 name > resource2 name
 * 0 if the same
 */
int comp_resource_name(const void* v1, const void* v2) {
    Resource* x1 = *((Resource**) v1);
    Resource* x2 = *((Resource**) v2);
    char* name1 = x1->name;
    char* name2 = x2->name;
    return strcmp(name1, name2);
}

/* Sort the neighbours by name
 * Return > 0 if neighbour1 name < neighbour2 name
 * < 0 if neighbour1 name > neighbour2 name
 * 0 if the same
 */
int comp_neighbour_name(const void* v1, const void* v2) {
    Neighbour* x1 = *((Neighbour**) v1);
    Neighbour* x2 = *((Neighbour**) v2);
    char* name1 = x1->name;
    char* name2 = x2->name;
    return strcmp(name1, name2);
}

/* This is for testing and just prints out all the deferred message struct
 * Keys
 */
void print_keys(Depot* depot) {
    for (int i = 0; i < depot->deferredCount; i++) {
        fprintf(stderr, "Key at %d:%s\n", i, depot->deferred[i]->key);
    }
}

/* Create a struct to store a pointer to Depot and a port
 * Return Client*
 */
Client* create_client_info(Depot* depot, char* port) {
    Client* client = malloc(sizeof(Client));
    client->depotCurrent = depot;
    client->port = port;
    return client;
}

int main(int argc, char** argv) {
    set_up_sigaction();
    DepotStatus result = check_args(argc, argv);
    Depot* depot = malloc(sizeof(Depot));
    initialise_depot(depot);
    globalDepot = depot;
    set_depot_name(depot, argv[1]);
    if (result != OK) {
        return show_message(result);
    }
    result = add_start_to_stockpile(argc, argv, depot);
    if (result != OK) {
        return show_message(result);
    }
    pthread_create(&depot->networkid, 0, create_server, depot);
    
    char* string;
    while (1) {
        if (read_channel(&depot->channel, (void**) &string)) {
            result = process_message(string, depot);
            free(string);
            fflush(stdout);
            fflush(stderr);
        }
    }
    
    //Wait for network thread to join
    void* temp;
    pthread_join(depot->networkid, &temp);
    return 0;
}

