OPTS = --std=gnu99 -pedantic -Wall -pthread
RM = rm -rf

.PHONY: all clean
.DEFAULT_GOAL := all

all: 2310depot

debug: OPTS += -g
debug: all

2310depot: 2310depot.o channel.o queue.o
	gcc $(OPTS) 2310depot.o channel.o queue.o -o 2310depot

2310depot.o: 2310depot.c channel.h
	gcc $(OPTS) -c 2310depot.c -o 2310depot.o

channel.o: channel.c channel.h queue.h
	gcc $(OPTS) -c channel.c -o channel.o

queue.o: queue.c queue.h
	gcc $(OPTS) -c queue.c -o queue.o

clean:
		$(RM) 2310depot *.o *~
